const {promisify} = require('util');
const fs = require('fs');
const writeFileAsync = promisify(fs.writeFile);
const readFileAsync = promisify(fs.readFile);

const postProcessSite = async ({localSite, urls, pathInS3}) => {
    try {
        console.log('starting post processing: ', localSite);
        let data = await readFileAsync(localSite, {encoding: 'utf8'})  
        if (!data) return new Error('no data to process');

        if (urls.length === 0 ) return;

        for (_url of urls ) {
            let { from, to } = _url;

             const url = from.split('?')[0];
             
             const { hostname } = new URL(url);
             const protocol = from.split(hostname)[0];
             const entryToReplace = protocol + hostname;
            
            let processedData = data.toLowerCase(); 
            while ( processedData.includes(entryToReplace)) {
                processedData = processedData.replace(entryToReplace, to);
                data = processedData;
            }
        }

        return writeFileAsync(localSite, data, 'utf8')        

    } catch(err) {
        console.log(err);
        return err;
    }
}

 module.exports =  { postProcessSite };
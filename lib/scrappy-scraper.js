
const {promisify} = require('util');
const fs = require('fs');
const writeFileAsync = promisify(fs.writeFile);
const fse = require('fs-extra');
const path = require('path');

const log = {}

const scrapePage = async ({page, siteUrl, dest}) => {
    try {
    const _url = new URL(siteUrl);
    const urlsToReplace = [];
    page.on('response', async (response) => { 
        /**
         * Output folders structure:
         * output
         *  -- index.html
         *  -- local folders 
         *      -- folders / files
         *  -- EXTERNAL
         *      -- www.domain.com/folder
         *           -- folders / files
         */

         const status = response.status();
         const { pathname, href, hostname} = new URL(response.url());  

         log[href] = status;

        if (status == 302 ) return;

            

        let from = href;
        let filepath;
        let filePath;
        let to = hostname;

        if (_url.hostname !== hostname ) {
            filePath = path.resolve(`./output/external__/${hostname}${pathname}`);
            filepath = path.join(`/external__/${hostname}${pathname}`);
            to = path.join('external__/'+hostname);
        } else {
            filePath = path.resolve(`./output${pathname}`);
            if (pathname === '/') return;
            filepath = pathname;
        }

        urlsToReplace.push({ from, filepath, to });
        try {
            return await fse.outputFile(filePath, await response.buffer());
        } catch (err) {
            return err;
          }
    });

    await page.goto(siteUrl, {waitUntil: 'networkidle2'});
    
    await page.screenshot({path: path.join(dest,'screenshot.png'),
    clip: {x: 0, y:0, width: 1024, height: 800}
    });
    
    const content = await page.content();
    await writeFileAsync(path.join(dest, 'index.html'), content);

    await writeFileAsync(path.join(dest, 'original-index.html'), content);
    await writeFileAsync(path.join(dest, 'log.text'), JSON.stringify(log));
    return  urlsToReplace;

    } catch (err) {
      console.log('Error while trying to scrape: [' +  siteUrl + '] Error Message: ' + err );
      return err;
    }
}

module.exports =  { scrapePage };
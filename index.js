const puppeteer = require('puppeteer');
const path = require('path');
const fse = require('fs-extra');
const { scrapePage } = require('./lib/scrappy-scraper');
const { postProcessSite } = require('./lib/post-processor');

// API: 
/**
 * 
 * @param {*} siteUrl 
 * @param {*} dest - the local folder for site resourcess 
 * @param {*} pathInS3 - supply the path of the uploaded output folder in s3
 * @param {*} headless - browser mode
 */
const start = async (siteUrl, dest, pathInS3=null, headless=true) => {
    if (!siteUrl) throw new Error('[Please supply a valid url]');
    let browser;
    try {
        browser = await puppeteer.launch({headless, timeout: 2000, args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]});
        const page = await browser.newPage();
        console.time('scrapping');
        const urls = await scrapePage({page, siteUrl, dest});
        console.timeEnd('scrapping');
        console.time('post-processing');
        const localSite = path.join(dest, 'index.html');
        await postProcessSite({localSite, urls, pathInS3});
        console.timeEnd('post-processing');
        return 'done';
    } catch( err) {
        console.log(err);
        return err;
    }
}

const curDir = path.join(__dirname, 'output');
fse.removeSync(curDir);
fse.ensureDirSync(curDir);
const url = process.argv[2];

// Use: 
start(url, curDir)
.then((done) => { 
    console.info(done);
    process.exit();
})
.catch(console.error);
